import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  
  slideOpts = {
    effect: 'flip'
  };
  public appPages = [
    {
      title: 'Inicio',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Hechos historicos',
      url: '/hechos',
      icon: 'md-calendar'
    },
    {
      title: 'Ubicación',
      url: '/ubicacion',
      icon: 'ios-navigate'
    },
    {
      title: 'Clima',
      url: '/clima',
      icon: 'md-cloudy-night'
    },
    {
      title: 'Flora',
      url: '/flora',
      icon: 'md-flower'
    },
    {
      title: 'Fauna',
      url: '/fauna',
      icon: 'ios-bug'
    },
    {
      title: 'Población',
      url: '/poblacion',
      icon: 'ios-people'
    },
    {
      title: 'Religión',
      url: '/religion',
      icon: 'ios-book'
    },
    {
      title: 'Economía',
      url: '/economia',
      icon: 'logo-usd'
    },
    {
      title: 'Tradiciones',
      url: '/tradiciones',
      icon: 'md-flame'
    },
    {
      title: 'Artesanías',
      url: '/artesanias',
      icon: 'ios-cafe'
    },
    {
      title: 'Gastronomía',
      url: '/gastronomia',
      icon: 'md-pizza'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
