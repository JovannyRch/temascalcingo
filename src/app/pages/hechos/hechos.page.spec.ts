import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HechosPage } from './hechos.page';

describe('HechosPage', () => {
  let component: HechosPage;
  let fixture: ComponentFixture<HechosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HechosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HechosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
