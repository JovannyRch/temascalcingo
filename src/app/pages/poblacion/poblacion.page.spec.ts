import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoblacionPage } from './poblacion.page';

describe('PoblacionPage', () => {
  let component: PoblacionPage;
  let fixture: ComponentFixture<PoblacionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoblacionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoblacionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
