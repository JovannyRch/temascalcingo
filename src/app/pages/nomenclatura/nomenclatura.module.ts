import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NomenclaturaPage } from './nomenclatura.page';

const routes: Routes = [
  {
    path: '',
    component: NomenclaturaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [NomenclaturaPage]
})
export class NomenclaturaPageModule {}
