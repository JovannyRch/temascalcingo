import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtesaniasPage } from './artesanias.page';

describe('ArtesaniasPage', () => {
  let component: ArtesaniasPage;
  let fixture: ComponentFixture<ArtesaniasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtesaniasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtesaniasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
